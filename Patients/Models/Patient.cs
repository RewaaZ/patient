﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Patients.Models
{
    public class Patient
    {
        [Key]
        public int PId { get; set; }
        [Required]
        [Column (TypeName = "nvarchar(100)")]
        public string Name { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(50)")]
        public string Date { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(20)")]
        public string Insurance { get; set; }
        
        [Column(TypeName = "nvarchar(100)")]
        public string Description { get; set; }


    }
}
