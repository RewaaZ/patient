﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patients.Migrations
{
    public partial class modification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndtDate",
                table: "patients");

            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "patients",
                newName: "Date");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Date",
                table: "patients",
                newName: "StartDate");

            migrationBuilder.AddColumn<string>(
                name: "EndtDate",
                table: "patients",
                type: "nvarchar(10)",
                nullable: true);
        }
    }
}
